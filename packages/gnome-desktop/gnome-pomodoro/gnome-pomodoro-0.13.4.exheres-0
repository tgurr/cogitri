# Copyright 2017 Rasmus Thomsen
# Distributed under the terms of the GNU General Public License v2

require github [ user=codito ] vala [ vala_dep=true with_opt=true ]
require gtk-icon-cache gsettings
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 ] ]

SUMMARY="A time management utility for GNOME based on the pomodoro technique"
HOMEPAGE="http://gnomepomodoro.org/"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-util/desktop-file-utils
        sys-devel/gettext[>=0.19.6]
        virtual/pkg-config[>=0.22]
    build+run:
        core/json-glib
        dev-libs/appstream-glib
        dev-libs/dbus-glib:1
        dev-libs/glib:2[>=2.38.0]
        dev-libs/libpeas:1.0[>=1.5.0]
        gnome-desktop/gobject-introspection:1[>=1.36.0]
        media-libs/gstreamer:1.0
        media-libs/libcanberra[>=0.30]
        media-plugins/gst-plugins-base:1.0
        x11-libs/cairo
        x11-libs/gtk+:3[>=3.20.0]
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'vapi vala'
)

# Require X
RESTRICT="test"

src_prepare() {
    edo autopoint --force
    autotools_src_prepare
}

pkg_postrm() {
    gtk-icon-cache_pkg_postrm
    gsettings_pkg_postrm
}

pkg_postinst() {
    gtk-icon-cache_pkg_postinst
    gsettings_pkg_postinst
}

